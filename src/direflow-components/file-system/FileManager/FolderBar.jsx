import React, { useState } from "react";
import { ListItem, List} from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import clsx from "clsx";

export default function FolderList(props){
    const useStyles = makeStyles(theme => ({
        root: {
            padding: '10px 0px',
            '& .folderItem':{
                display:'block !important',
                width: '100%',
                margin:'0px !important',
                padding:'0px',
                fontSize: "13px",

                '& .folderTitle':{
                    position: 'relative',
                    '& .iconArrow':{
                        position: 'absolute',
                        left:'0px',
                        top: '0px',
                        fontSize: '10px',
                        lineHeight: '17px',
                        padding: '6px 5px'
                    },
                    '& .titleWrap':{
                       display:'block',
                       width: '100%',
                       padding: '5px 0px'
                    },
                    '& .title':{
                        padding: '0px 0px 0px 7px'
                    },
                },
                '& .MuiButtonBase-root': {
                    padding: '0px 0px 0px 20px',
                    borderRadius: '3px'
                },
                '& .folderSubmenu': {
                    display: 'none',
                    width: '100%',
                    padding:'0px 0px 0px 10px !important',
                    margin:'0px !important'
                },
                '&.active > .MuiButtonBase-root': {
                    background: '#0492f2',
                    color: '#fff'
                },
                '&.open > .folderSubmenu': {
                    display: 'block',
                },
                '&.open > .MuiButtonBase-root .iconArrow': {
                    transform: 'rotate(90deg)'
                }
            }
        }
    }));

    const classes = useStyles();
    const { foldersList, onFolderClick, selectedFolder } = props;
    return (
            <div className={classes.root} key={`folderRoot`} >
                {foldersList.name && (
                    <MenuItem item={foldersList} onFolderClick={onFolderClick} currentUrl={selectedFolder} />
                )}
            </div>
    )

}


function MenuSubmenu(props){
    const { item, currentUrl, onFolderClick } = props;

    return (
        <List className='folderSubmenu'>
        {item.children.map((child, index) => (
          <React.Fragment key={index}>
            {child.name && (
              <MenuItem item={child} onFolderClick={onFolderClick} parentItem={item} currentUrl={currentUrl} />
            )}
          </React.Fragment>
        ))}
      </List>
    );

}


function MenuItem(props) {
    const asideLeftLIRef = React.createRef();
    const { item, currentUrl, onFolderClick} = props;
    const [expand, setExpand] = useState(false);

    const mouseClick = () => {
        onFolderClick(item.path);
    }
    const handleExpand = () => {
        setExpand(!expand);
    }

    const isMenuItemIsActive = item => {
        if (item.children && item.children.length > 0) {
        isMenuRootItemIsActive(item);
        }
        return currentUrl.indexOf(item.path) !== -1;
    };

    const isMenuRootItemIsActive = item => {
        for (const subItem of item.children) {
        if (isMenuItemIsActive(subItem)) {
            return true;
        }
        }
        return false;
    };

    const isActive = isMenuItemIsActive(item);

    return (
      <ListItem
        ref={asideLeftLIRef}
        className={clsx(
          'folderItem',
          {
            "open": isActive && item.children || expand,
            "active": item.path === currentUrl
          }
        )}
      >
        <ListItem button className="folderTitle" >
            {item.children.length > 0 &&
                <div className={"iconArrow"}>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg"     width="1em" height="1em"
                         viewBox="0 0 1024 1024" onClick={handleExpand} >
                        <title></title>
                        <g id="icomoon-ignore">
                        </g>
                        <path fill={"#3f51b5"}
                              d={"M776.279 483.904l-472.843-472.843c-15.654-15.118-40.598-14.684-55.716 0.97-14.749 15.27-14.749 39.478 0 54.746l444.985 444.985-444.985 444.985c-15.384 15.386-15.384 40.33 0 55.716 15.388 15.384 40.33 15.384 55.716 0l472.843-472.843c15.384-15.388 15.384-40.33 0-55.716z"}
                        />
                    </svg>
                </div>
          }
            <span className="titleWrap"  onClick={mouseClick}>
                {isActive && item.children ?
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg"     width="1em" height="1em"
                                                 viewBox="0 0 1024 1024">
                    <title></title>
                    <g id="icomoon-ignore">
                    </g>
                    <path  fill={"#3f51b5"} d="M997.802 339.526c-17.894-18.524-42.138-28.718-68.239-28.718h-835.144c-26.088 0-50.333 10.206-68.239 28.718-17.714 18.31-27.010 42.442-26.122 68.621l24.706 462.509c1.776 49.535 44.119 89.831 94.395 89.831h784.563c49.345 0 90.765-38.936 94.339-89.169l25.886-463.847c0.852-25.49-8.432-49.625-26.146-67.945zM930.777 867.048c-1.046 14.568-12.926 25.998-27.054 25.998h-784.563c-14.152 0-26.516-11.364-27.020-25.402l-24.696-462.419c-0.236-7.070 2.326-13.746 7.228-18.816 5.080-5.26 12.106-8.16 19.76-8.16v0.012h835.132c7.654 0 14.68 2.9 19.772 8.16 4.9 5.058 7.464 11.746 7.262 18.062l-25.82 462.565zM883.478 141.070h-370.43l-89.449-62.305c-11.072-9.734-25.964-15.252-41.42-15.252h-243.911c-43.387 0-78.681 35.8-78.681 79.805v201.198h67.441v-201.198c0-6.936 4.934-12.364 11.24-12.364h242.495c0.754 0.652 1.552 1.248 2.372 1.82l100.037 69.689c5.666 3.946 12.386 6.058 19.276 6.058h381.030c6.306 0 11.24 5.428 11.24 12.364v123.641h67.441v-123.653c0-44.003-35.304-79.803-78.681-79.803z"
                    />
                </svg>  :
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg"     width="1em" height="1em"
                         viewBox="0 0 1024 1024">
                        <title></title>
                        <g id="icomoon-ignore">
                        </g>
                        <path fill={"#3f51b5"}
                            d="M960 211.2h-470.182l-125.658-129.178c-7.232-7.424-17.152-11.622-27.52-11.622h-272.64c-35.29 0-64 28.71-64 64v755.2c0 35.29 28.71 64 64 64h896c35.29 0 64-28.71 64-64v-614.4c0-35.29-28.71-64-64-64zM947.2 876.8h-870.4v-729.6h243.622l125.658 129.178c7.232 7.424 17.152 11.622 27.52 11.622h473.6v588.8z"

                        />
                    </svg>
                }
                <span className="title">{item.name}</span>
            </span>
        </ListItem>

        {item.children.length > 0 && <MenuSubmenu item={item} onFolderClick={onFolderClick} parentItem={item} currentUrl={currentUrl}  /> }

      </ListItem>
    );
}
