import React from 'react';
import {Tooltip, Button, ButtonGroup} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
import icoMoonConfig from '../../Data/Cronicon.json';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    '& > *': {
      margin: theme.spacing(1),
    },
      '& > span': {
          margin: theme.spacing(2),
      },
  },
  button: {
      fontSize:'14px',
      padding:'10px 5px',
      minWidth: '35px',
      background: '#fff',
      '& span':{

      }
  },
  buttonTitle: {
    fontSize: '12px',
    textTransform: 'none',
    lineHeight: '11px',
    padding: '0px 5px'

  }
}));

export default function ButtonGroupSimple(props){
  const classes = useStyles();
  const { buttons, index } = props;
  return (
    <div className={classes.root}>
      <ButtonGroup key={index} color="primary" aria-label="outlined primary button group">
        {buttons.map((button, index)=>{
            return button.disable ?
                          <Button  key={index} className={classes.button} disabled={true} aria-haspopup="true">
                            {/*{button.icon && <span className={`${button.icon}`}></span>}*/}
                              {/*<Icon>{button.icon}</Icon>*/}
                              <svg version="1.1" xmlns="http://www.w3.org/2000/svg"     width="1em" height="1em"
                                   viewBox="0 0 1024 1024">
                                  <title></title>
                                  <g id="icomoon-ignore">
                                  </g>
                                  <path fill={"rgba(0, 0, 0, 0.26)"} d={`${button.icon}`}/>
                              </svg>


                          </Button>
                          :
                          <Tooltip key={index} title={button.title} aria-label={button.title}>
                            <Button  className={classes.button} onClick={button.onClick} disabled={button.disable} aria-haspopup="true">
                              {/*{button.icon && <span className={`${button.icon}`}></span>}*/}
                                {/* <span className={classes.buttonTitle}>{button.title}</span> */}
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"     width="1em" height="1em"
                                     viewBox="0 0 1024 1024">
                                    <title></title>
                                    <g id="icomoon-ignore">
                                    </g>
                                    <path fill={"#3f51b5"} d={`${button.icon}`}/>
                                </svg>
                            </Button>
                          </Tooltip>

        })}
      </ButtonGroup>
    </div>
  );
}
