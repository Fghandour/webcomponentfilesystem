import { DireflowComponent } from 'direflow-component';
import dirflowIndex from './dirflowIndex';
export default DireflowComponent.create({
  component: dirflowIndex,
  configuration: {
    tagname: 'file-system',
  },
  plugins: [
    {
      name: 'font-loader',
      options: {
        google: {
          families: ['Advent Pro', 'Noto Sans JP'],
        },
        custom: {
          urls:['https://fonts.googleapis.com/icon?family=Material+Icons','https://use.fontawesome.com/releases/v5.12.0/css/all.css']
        },
      },
    },
    {
      name: 'icon-loader',
      options: {
        packs: ['material-icons','fa']
      }
    },
    {
      name: 'material-ui',
    },

  ],
});
